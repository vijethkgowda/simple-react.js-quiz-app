let questions = [
  {
    question: 'Where is Taj Mahal?',
    options: ['Agra', 'Mumbai', 'Delhi', 'Bangalore'],
    answer: 0
  },
  {
    question: 'Where is Taj Hotel?',
    options: ['Agra', 'Mumbai', 'Delhi', 'Bangalore'],
    answer: 1
  },
  {
    question: 'Where is Lalbaag?',
    options: ['Agra', 'Mumbai', 'Delhi', 'Bangalore'],
    answer: 3
  },
  {
    question: 'Where is Red fort?',
    options: ['Agra', 'Mumbai', 'Delhi', 'Bangalore'],
    answer: 2
  },
  {
    question: 'Where is Vijeth?',
    options: ['Agra', 'Shiroor', 'Delhi', 'Bangalore'],
    answer: 1
  },
]

export default questions
import React from 'react'
import Lottie from "react-lottie"
import { Link } from 'react-router-dom'
import Navbar from './Navbar'

const Dashboard = () => {

  return (
    <>
    <Navbar></Navbar>
      <div className="flex flex-col">
        <div className="w-4/12 ml-auto mr-auto">
          <Link to='/questions'>
            <div className="flex-row">
              <div className="">
                <Lottie options={{
                  loop: true,
                  autoplay: true,
                  animationData: {
                    "v": "5.5.10", "fr": 30, "ip": 0, "op": 60, "w": 300, "h": 300, "nm": "Fast Forward", "ddd": 0, "assets": [], "layers": [{
                      "ddd": 0, "ind": 1, "ty": 4, "nm": "Path 11", "sr": 1, "ks": { "o": { "a": 0, "k": 100, "ix": 11 }, "r": { "a": 0, "k": 0, "ix": 10 }, "p": { "a": 0, "k": [122.668, 150.5, 0], "ix": 2 }, "a": { "a": 0, "k": [0, 0, 0], "ix": 1 }, "s": { "a": 0, "k": [100, 100, 100], "ix": 6 } }, "ao": 0, "shapes": [{
                        "ty": "gr", "it": [{ "ind": 0, "ty": "sh", "ix": 1, "ks": { "a": 0, "k": { "i": [[0, 0], [0, 0], [0, 0]], "o": [[0, 0], [0, 0], [0, 0]], "v": [[-19.668, -26.5], [-19.668, 26.5], [19.672, 0]], "c": true }, "ix": 2 }, "nm": "Path 1", "mn": "ADBE Vector Shape - Group", "hd": false }, {
                          "ty": "st", "c": {
                            "a": 0, "k":
                              [0.1, 0.2, 0.44, 1], "ix": 3
                          }, "o": { "a": 0, "k": 100, "ix": 4 }, "w": { "a": 0, "k": 5.36, "ix": 5 }, "lc": 2, "lj": 2, "bm": 0, "nm": "Stroke 1", "mn": "ADBE Vector Graphic - Stroke", "hd": false
                        }, { "ty": "tr", "p": { "a": 0, "k": [0, 0], "ix": 2 }, "a": { "a": 0, "k": [0, 0], "ix": 1 }, "s": { "a": 0, "k": [100, 100], "ix": 3 }, "r": { "a": 0, "k": 0, "ix": 6 }, "o": { "a": 0, "k": 100, "ix": 7 }, "sk": { "a": 0, "k": 0, "ix": 4 }, "sa": { "a": 0, "k": 0, "ix": 5 }, "nm": "Transform" }], "nm": "Path 10", "np": 2, "cix": 2, "bm": 0, "ix": 1, "mn": "ADBE Vector Group", "hd": false
                      }, { "ty": "tm", "s": { "a": 1, "k": [{ "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 10, "s": [0] }, { "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 15, "s": [21] }, { "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 40, "s": [21] }, { "t": 45, "s": [0] }], "ix": 1 }, "e": { "a": 0, "k": 100, "ix": 2 }, "o": { "a": 1, "k": [{ "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 10, "s": [0] }, { "t": 45, "s": [-720] }], "ix": 3 }, "m": 1, "ix": 2, "nm": "Trim Paths 1", "mn": "ADBE Vector Filter - Trim", "hd": false }], "ip": 0, "op": 60, "st": 0, "bm": 0
                    }, {
                      "ddd": 0, "ind": 2, "ty": 4, "nm": "Path 10 Copy 2", "sr": 1, "ks": { "o": { "a": 0, "k": 100, "ix": 11 }, "r": { "a": 0, "k": 0, "ix": 10 }, "p": { "a": 0, "k": [162.668, 150.5, 0], "ix": 2 }, "a": { "a": 0, "k": [0, 0, 0], "ix": 1 }, "s": { "a": 0, "k": [100, 100, 100], "ix": 6 } }, "ao": 0, "shapes": [{
                        "ty": "gr", "it": [{ "ind": 0, "ty": "sh", "ix": 1, "ks": { "a": 0, "k": { "i": [[0, 0], [0, 0], [0, 0]], "o": [[0, 0], [0, 0], [0, 0]], "v": [[-19.668, -26.5], [-19.668, 26.5], [19.672, 0]], "c": true }, "ix": 2 }, "nm": "Path 1", "mn": "ADBE Vector Shape - Group", "hd": false }, {
                          "ty": "st", "c": {
                            "a": 0, "k":
                              [0.14, 0.34, 0.86, 1], "ix": 3
                          }, "o": { "a": 0, "k": 100, "ix": 4 }, "w": { "a": 0, "k": 5.36, "ix": 5 }, "lc": 1, "lj": 2, "bm": 0, "nm": "Stroke 1", "mn": "ADBE Vector Graphic - Stroke", "hd": false
                        }, { "ty": "tr", "p": { "a": 0, "k": [0, 0], "ix": 2 }, "a": { "a": 0, "k": [0, 0], "ix": 1 }, "s": { "a": 0, "k": [100, 100], "ix": 3 }, "r": { "a": 0, "k": 0, "ix": 6 }, "o": { "a": 0, "k": 100, "ix": 7 }, "sk": { "a": 0, "k": 0, "ix": 4 }, "sa": { "a": 0, "k": 0, "ix": 5 }, "nm": "Transform" }], "nm": "Path 10 Copy", "np": 2, "cix": 2, "bm": 0, "ix": 1, "mn": "ADBE Vector Group", "hd": false
                      }, { "ty": "tm", "s": { "a": 1, "k": [{ "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 10, "s": [0] }, { "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 15, "s": [21] }, { "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 40, "s": [21] }, { "t": 45, "s": [0] }], "ix": 1 }, "e": { "a": 0, "k": 100, "ix": 2 }, "o": { "a": 1, "k": [{ "i": { "x": [0.667], "y": [1] }, "o": { "x": [0.333], "y": [0] }, "t": 10, "s": [30] }, { "t": 45, "s": [-720] }], "ix": 3 }, "m": 1, "ix": 2, "nm": "Trim Paths 1", "mn": "ADBE Vector Filter - Trim", "hd": false }], "ip": 0, "op": 60, "st": 0, "bm": 0
                    }], "markers": []
                  },
                  rendererSettings: {
                    preserveAspectRatio: "xMidYMid slice"
                  }
                }}
                  height={400}
                  width={450}
                ></Lottie>
              </div>
            </div>
          </Link>
        </div>
        <div className="w-1/12 ml-auto mr-auto text-4xl -mt-32 font-bold">
          <Link to='/questions'>
            Play
        </Link>
        </div>
      </div>
    </>
  )

}

export default Dashboard

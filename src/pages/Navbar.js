import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {

  return (
    <header className="bg-teal-400 py-2">
      <div className="flex justify-between max-w-4xl mx-auto px-6 items-center">
        <div>
          <Link className="text-white font-medium" to='/'>Home</Link>
        </div>
        <div>
          <Link
            className="text-2xl text-green-700 font-bold mx-4"
            to='/addQuestion'>
            +?
        </Link>
          <Link
            className="text-2xl text-red-600 font-bold"
            to='/deleteQuestion'>
            -?
        </Link>
        </div>
      </div>
    </header>
  )
}
export default Navbar

import React, { useState, useEffect } from 'react'
import Navbar from './Navbar'

const AddQuestion = () => {
  const [question, setQuestion] = useState('')
  const [op1, setOp1] = useState('')
  const [op2, setOp2] = useState('')
  const [op3, setOp3] = useState('')
  const [op4, setOp4] = useState('')
  const [ans, setAns] = useState('')

  const Add = () => {
    let existing = JSON.parse(localStorage.getItem("localQuestions"))
    let obj = {
      question: question,
      options: [op1, op2, op3, op4],
      answer: ans
    }

    let allQuestions = []

    allQuestions.push(existing, obj)

    localStorage.setItem("localQuestions",JSON.stringify(allQuestions))

    console.log(JSON.parse(localStorage.getItem("localQuestions")))
  }

  useEffect(() => {
    Add
  }, [])

  return (
    <>
    <Navbar></Navbar>
      <form className="w-full max-w-full my-10 mx-5 font-sans">

        <div className="md:flex md:items-center mb-3">
          <div className="md:w-1/3 ml-12">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Question:
            </label>
          </div>

          <div className="md:w-9/12">
            <textarea required
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 h-24 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500"
              id="template"
              onChange={e => { setQuestion(e.target.value) }}
            />
          </div>
        </div>

        <div className="md:flex md:items-center mb-3">
          <div className="md:w-1/3">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Option A:
            </label>
          </div>
          <div className="md:w-2/3">
            <input type="text" required
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500"
              onChange={e => { setOp1(e.target.value) }}>
            </input>
          </div>
        </div>

        <div className="md:flex md:items-center mb-3">
          <div className="md:w-1/3">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Option B:
            </label>
          </div>
          <div className="md:w-2/3">
            <input type="text" required
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500"
              onChange={e => { setOp2(e.target.value) }}>
            </input>
          </div>
        </div>

        <div className="md:flex md:items-center mb-3">
          <div className="md:w-1/3">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Option C:
            </label>
          </div>
          <div className="md:w-2/3">
            <input type="text" required
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500"
              onChange={e => { setOp3(e.target.value) }}>
            </input>
          </div>
        </div>

        <div className="md:flex md:items-center mb-3">
          <div className="md:w-1/3">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Option D:
            </label>
          </div>
          <div className="md:w-2/3">
            <input type="text" required
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500"
              onChange={e => { setOp4(e.target.value) }}>
            </input>
          </div>
        </div>

        <div className="md:flex md:items-center">
          <div className="md:w-1/3">
            <label className="block text-gray-900 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Select Answer:
            </label>
          </div>
          <div className="md:w-2/3">
            <select required
              onChange={e => { setAns(Number(e.target.value)) }}
              className="bg-gray-100 appearance-none border-2 border-gray-400 rounded w-9/12 py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-teal-500">
              <option disabled selected>Select answer option</option>
              <option value={0}>A</option>
              <option value={1}>B</option>
              <option value={2}>C</option>
              <option value={3}>D</option>
            </select>
          </div>
        </div>

        <div className="md:flex md:items-center">
          <div className="md:w-1/3"></div>
          <div className="md:w-2/3">
            <button
              onClick={e => { e.preventDefault(), Add() }}
              className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-4 mt-4 ml-auto mr-auto w-3/4 rounded"
              type="submit">
              Add
            </button>
          </div>
        </div>

      </form>
    </>
  )
}

export default AddQuestion

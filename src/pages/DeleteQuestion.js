import React, { useState, useEffect } from 'react'
import Navbar from './Navbar'

const DeleteQuestion = () => {
  let questions = JSON.parse(localStorage.getItem("localQuestions"))

  const [selectQuestion, setSelectedQuestion] = useState('')

  const Delete = () => {
    console.log(selectQuestion)
  }


  useEffect(()=>{
    Delete()
  },[selectQuestion])

  return (
    <>
    <Navbar></Navbar>
      <div className="flex flex-row">
        <div className="w-8/12">
          {
            questions.map(question => {
              return (
                <div className="flex flex-col">
                  <div className="w-8/12 mr-auto ml-auto mt-6 text-base">
                    {question.question}
                  </div>
                  <div className="flex flex-auto w-8/12 mr-auto ml-auto">
                    <div className="mr-6 pl-4 rounded w-1/2 text-base">
                      Answer :  <span className="text-green-800 font-semibold text-xl">{question.options[question.answer]} </span>
                    </div>
                    <div className="mr-6 pl-4 rounded w-1/2 text-base">
                      <button 
                      onClick={e => {setSelectedQuestion(question.question),Delete()}}
                      className="bg-red-400 px-2 py-1 rounded hover:bg-red-600 border-none focus:border-none"
                      >Delete</button>
                    </div>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    </>
  )
}

export default DeleteQuestion

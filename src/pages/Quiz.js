import React from 'react'

const Quiz = ({ question, options, next, validate, check, op1, op2, op3, op4 }) => {

  return (
    <>
      <div className="flex-row">
        <div className="w-8/12 mr-auto ml-auto mt-8 text-lg">
          <span className="ml-4">{question}</span>
        </div>
        {
          check ?
            <span>
              <div className="flex flex-col w-8/12 mr-auto ml-auto mt-6">
                <div className={op1}>
                  <button className="w-full border-none focus:outline-none">{options[0]}</button>
                </div>
                <div className={op2}>
                  <button className="w-full border-none focus:outline-none">{options[1]}</button>
                </div>
                <div className={op3}>
                  <button className="w-full border-none focus:outline-none">{options[2]}</button>
                </div>
                <div className={op4}>
                  <button className="w-full border-none focus:outline-none">{options[3]}</button>
                </div>
              </div>

              <div className="w-8/12 mr-auto ml-auto mt-8">
                <div>
                  <button
                    onClick={e => {
                      next()
                    }}
                    className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-3 px-4 mt-1 w-full rounded">Next</button>
                </div>
              </div>
            </span>
            :
            <div className="flex flex-col w-8/12 mr-auto ml-auto mt-6">
              <div>
                <button
                  onClick={e => {
                    validate(0)
                  }}
                  className="border-none focus:outline-none mr-6 mt-1 py-4 px-4 bg-green-200 hover:bg-green-300 rounded w-full">{options[0]}</button>
              </div>
              <div>
                <button
                  onClick={e => {
                    validate(1)
                  }}
                  className="border-none focus:outline-none mr-6 mt-1 py-4 px-4 bg-green-200 hover:bg-green-300 rounded w-full">{options[1]}</button>
              </div>
              <div>
                <button
                  onClick={e => {
                    validate(2)
                  }}
                  className="border-none focus:outline-none mr-6 mt-1 py-4 px-4 bg-green-200 hover:bg-green-300 rounded w-full">{options[2]}</button>
              </div>
              <div>
                <button
                  onClick={e => {
                    validate(3)
                  }}
                  className="border-none focus:outline-none mr-6 mt-1 py-4 px-4 bg-green-200 hover:bg-green-300 rounded w-full">{options[3]}</button>
              </div>
            </div>
        }


      </div>
    </>
  )
}

export default Quiz

import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import Quiz from './Quiz'

const Questions = () => {
  let questions = JSON.parse(localStorage.getItem("localQuestions"))
  console.log(questions)
  let history = useHistory()
  const [questionIndex, setQuestionIndex] = useState(0)
  const [index, setIndex] = useState()
  const [check, setCheck] = useState(false)
  const [count, setCount] = useState(0)

  let other = "mr-6 mt-1 py-4 px-4 bg-green-200 rounded w-full"

  const [op1, setOp1] = useState(other)
  const [op2, setOp2] = useState(other)
  const [op3, setOp3] = useState(other)
  const [op4, setOp4] = useState(other)

  let correct = "mr-6 mt-1 py-4 px-4 bg-green-500 rounded w-full"
  let wrong = "mr-6 mt-1 py-4 px-4 bg-red-500 rounded w-full"

  let length = questions.length - 1
  let ans = questions[questionIndex].answer

  const checkAns = (index) => {
    if (index == ans) {
      setCount(count + 1)
    }

    if (ans == 0) {
      setOp1(correct)
    }
    if (ans == 1) {
      setOp2(correct)
    }
    if (ans == 2) {
      setOp3(correct)
    }
    if (ans == 3) {
      setOp4(correct)
    }

    if (index == 0) {
      if (ans == 0) {
        setOp1(correct)
      }
      else {
        setOp1(wrong)
      }
    }
    if (index == 1) {
      if (ans == 1) {
        setOp2(correct)
      }
      else {
        setOp2(wrong)
      }
    }
    if (index == 2) {
      if (ans == 2) {
        setOp3(correct)
      }
      else {
        setOp3(wrong)
      }
    }
    if (index == 3) {
      if (ans == 3) {
        setOp4(correct)
      }
      else {
        setOp4(wrong)
      }
    }
  }

  const score = () => {
    let count = 0
    if (index == questions[questionIndex].answer) {
      count = count + 1
    }
    resultPage(count)
  }

  const resultPage = () => {
    if (questionIndex >= length) {
      history.replace(`/result/${count}`)
    }
  }

  useEffect(() => {
    checkAns()
  }, [])

  return (
    <>
      <div className="my-4">
        <Quiz
          key={questions[questionIndex].question}
          question={questions[questionIndex].question}
          options={questions[questionIndex].options}
          ans={questions[questionIndex].answer}
          next={() => {
            setQuestionIndex(questionIndex + 1)
            setCheck(false)
            setOp1(other)
            setOp2(other)
            setOp3(other)
            setOp4(other)
            score()
          }}
          validate={(ind) => {
            setIndex(ind)
            setCheck(true)
            checkAns(ind)
          }}
          check={check}
          op1={op1}
          op2={op2}
          op3={op3}
          op4={op4}
        />
      </div>
    </>
  )

}

export default Questions

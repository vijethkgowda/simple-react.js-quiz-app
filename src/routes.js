import React from 'react'
import { Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from 'history';

import Dashboard from './pages/Dashboard'
// import Quiz from './pages/Quiz'
import Questions from './pages/Questions'
import Result from './pages/Result' 
import AddQuestion from './pages/AddQuestion'
import DeleteQuestion from  './pages/DeleteQuestion'

const history = createBrowserHistory();

const Routes = () => (
  <Router history={history}>
    <Switch>
      {/* <Route exact path="/quiz" component={Quiz} /> */}
      <Route exact path="/questions" component={Questions} />
      <Route path="/result" component={Result} />
      <Route exact path="/addQuestion" component={AddQuestion} />
      <Route exact path="/deleteQuestion" component={DeleteQuestion} />
      <Route exact path="/" component={Dashboard} />
    </Switch>
  </Router>
)

export default Routes